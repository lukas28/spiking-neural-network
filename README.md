# Spiking Neural Network

Using the third generation artificial neuron to build a spiking neural network for handwritten digit classification.
The project utilites the simplified spiking neuron proposed in this paper: https://core.ac.uk/download/pdf/81263112.pdf.

## Spiking Neuron Architecture
Different than usual artificial neurons, spiking neurons do not transport the information based on real value integers or floating point 
numbers, the information is coded in the frequency of spikes. A spike could be seen as an impuls in a discrete point in time. Based on the 
spiking frequency which is used by the neuron, depending on the input and learning, different inputs can be distinguished. In this case 
different input images, showing digits from 0-9 are recognized by the network.

### Properties of the Leaky integrate and fire neuron (LIF)
The most important properties in a spiking neural network using the LIF neuron are:
1. Membrane Potential
Representing the actual potential to a certain point in time of a single neuron.

2. Minimum Potential
This is the lower bound value the neurons potential could possibly be.

3. Potential Threshold
This is the upper bound value the neurons potential should possibly be. If the membrane potential equals or exceeds this threshold,
the neuron spikes.

4. Refractory Potential
This is the value wich resets the membrane potential after the neuron spiked.

5. Refractory Period
After the neuron spiked, the membrane potential is reinitialized to the refractory potential and the neuron enters a waiting state.
This means, that the neuron potential stays constant for this time period, no matter how the input spikes look like.

6. Potential Decreasing Constant
In every timestep in a spiking neural network, the membrane potentials of the neurons increase according to the spike inputs and 
decrease by the potential decreasing constant.

### Behaviour of the LIF
Basically, the whole behavoir of the spiking neuron depends on the incomming spike inputs, the weights connecting a single neuron 
with all the neurons of the prevous layer and the potential decreasing constant. The formular that describes the membrane potential of
the neuron in every point in time is: $`P_t-1 + \sum_{i=1}^NW_i*S_it - D`$



## Receptive Field
The idea behind receptive fields is to give the machine kind of a human way of perceiving the invironment it is in. For this project, the classification of images
which show handwritten letters, this means, the receptive field is used to 'look' at the picture and feed the information into the spiking neural network. Therefore 
it works like a human eye which transfers which in contrast forwards the seen information into the brain for further processing.

### Off-centered field
In this type of receptive field, the information on the corners of the perceived area is more important than the information in the center. This is basically the exact 
opposite of the human eye.<br/>
![alt text](https://gitlab.com/lukas28/spiking-neural-network/raw/master/off_centered_receptive_field.png?inline=false "Off centered receptive field")

### On-centered field
This type if different to the human eye. Information in center of our visual field is sharper than on the edges. <br/>
![alt text](https://gitlab.com/lukas28/spiking-neural-network/raw/master/on_centered_receptive_field.png?inline=false "On centered receptive field")

## Learning - Spike-time dependent plasticity learning
Spike-time dependent plasticity learning means that connections (weights/synapses) to neurons which spike immediatelly before the neuron spikes are increased. If a 
connected neuron spikes immediately after the neuron spikes, the connection (weights/synapses) is decreased. As there is no supervision which tells the network whether
its results are right or wrong, this learning algorithm is an unsupervised learning algorithm.
For further information visit: Bi G-Q, Poo M-M. Synaptic modifications in cultured hippocampal neurons: dependence on spike timing, synaptic strength, and postsynaptic cell type. J. Neurosci. 1998; 18(24):10464.

# Results
For this project 784 input neurons are used. Each of these neurons corresponds to one pixel of the 28x28 input images from the MNIST handwritten digit dataset.
Equal to the number of different digits, 10 output neurons are implemented in the current setting. Each of them corresponding to one of the specific digits. For 
training one single picture for each digit was used. The timespan for training a single instance of an image was 150 timesteps with a timedelta of 1. For the results 
shown below and the weights which are added to the repository withing the weights.txt file, 10 training epochs were used.

## Weights
The following picture shows the trained weights. Basically a single weight is a connection from a specific output neuron to a specific input neuron and defines how 
important the spikes comming from that input neuron are for this specific neuron instance. In other words, each of the output neurons change the relevance to different 
pixels of the input image while training. We can determine the corresponding competences of each single output neuron by just looking at its weights.
![alt text](https://gitlab.com/lukas28/spiking-neural-network/raw/master/images/trainedWeights.png?inline=false "trained weights")


The following table shows the mappings between output neuron and digit. 

**Neuron** | **Digit** 
--- | --- 
1 | 5 
2 | 2 
3 | 1 
4 | 3 
5 | 9 
6 | 6 
7 | 4 
8 | 8 
9 | 0 
10 | 7 

## Membrane Potential of the single output neurons while training process
In this picture the process of spezialization for each output neuron while training is shown via the changing membrane potential patterns over time.
![alt text](https://gitlab.com/lukas28/spiking-neural-network/raw/master/images/membranePotentialWhileTraining.png?inline=false "training process")