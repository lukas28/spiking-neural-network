import Hyperparameters as hyp
import os
import numpy as np
import matplotlib.pyplot as plt

f = open('weights.txt', 'r')

content = f.read()

f.close()

weights = content.split('\n')
print(weights[0])

weightsTrained = []
for n in range(hyp.outputNeurons):
    counter = 0
    test = np.zeros((28, 28))
    values = weights[n].split(',')
    for i in range(28):
        for x in range(28):
            test[i][x] = float(values[counter])
            counter += 1
    weightsTrained.append(test)

f, axarr = plt.subplots(2, 5)
axarr[0, 0].matshow(weightsTrained[0])
axarr[0, 1].matshow(weightsTrained[1])
axarr[0, 2].matshow(weightsTrained[2])
axarr[0, 3].matshow(weightsTrained[3])
axarr[0, 4].matshow(weightsTrained[4])
axarr[1, 0].matshow(weightsTrained[5])
axarr[1, 1].matshow(weightsTrained[6])
axarr[1, 2].matshow(weightsTrained[7])
axarr[1, 3].matshow(weightsTrained[8])
axarr[1, 4].matshow(weightsTrained[9])

plt.show()