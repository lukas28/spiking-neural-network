# Implementation of the leaky integrate and fire model neuron which is a simplified version of the original SRM neuron


import math
#import numpy as np
import random


class SimplyfiedNeuron:

    def __init__(self):
        self.restingPotential = 0
        self.refractoryPotential = 0
        self.membranePotential = self.restingPotential
        self.minimalPotential = -5
        self.isSpiking = 0
        self.potentialDecreasingConstant = 0.75
        self.refractoryTime = 30

        self.potentialThreshold = 50

        self.presynapticNeurons = []
        self.presynapticWeights = {}

        self.lastSpikeTime = -1

        self.aminus = 0.3
        self.aplus = 0.8
        self.tplus = 10
        self.tminus = 10
        self.phi = 0.02

        self.wmax = 2
        self.wmin = -1.2

        self.potentialTrace = []
        self.spikeCount = 0



    def addPresynapticNeuron(self, neuron):
        self.presynapticNeurons.append(neuron)
        self.presynapticWeights[neuron] = random.uniform(0,self.wmax*0.5)

        #self.presynapticWeights[neuron] = 0.7

    def addPresynapticNeuronWithWeight(self, neuron, weight):
        self.presynapticNeurons.append(neuron)
        self.presynapticWeights[neuron] = weight


    def updatePotential(self, time, spikeTrain, synapses):
        '''check if neuron is resting'''
        if (time - self.lastSpikeTime) <= self.refractoryTime and self.lastSpikeTime != -1:
            # neuron ist im resting zustand
            self.potentialTrace.append(self.membranePotential)
            return

        '''check if membrane potential is within the boundaries and if so, update potential due to incomming spikes'''
        if self.membranePotential < self.potentialThreshold and self.membranePotential > self.minimalPotential:

            #new try
            #oldPotential = self.membranePotential
            #self.membranePotential = self.membranePotential + np.dot(synapses, spikeTrain)

            oldPotential = self.membranePotential
            sum = 0.0
            for neuron in self.presynapticNeurons:
                self.membranePotential += neuron.isSpiking * self.presynapticWeights[neuron]
                sum += neuron.isSpiking * self.presynapticWeights[neuron]
            if self.membranePotential > self.restingPotential: self.membranePotential -= self.potentialDecreasingConstant

            #if oldPotential != self.membranePotential:
                #print('es wurde was geändert ...')
                #print('differenz = ' + str(float(self.membranePotential-oldPotential)))
                #print('sum: ' + str(sum))
            self.potentialTrace.append(self.membranePotential)
            #print('updating')
            return

        '''check if potential is below minimum, and regulate back to resting potential if so'''
        if self.membranePotential <= self.minimalPotential:
            self.membranePotential = self.restingPotential
            self.potentialTrace.append(self.membranePotential)
            #print('holding')
            return


    def checkForSpike(self, time):
        if self.membranePotential >= self.potentialThreshold:
            self.lastSpikeTime = time
            self.spikeCount += 1
            #self.membranePotential = self.restingPotential
            #self.isSpiking = True
            return True
        else:
            return False

    def spikeIfPossible(self, time):
        if self.membranePotential >= self.potentialThreshold:
            self.membranePotential = self.restingPotential
            self.lastSpikeTime = time
            return True
        else:
            return False

    def changeWeight(self, time):
        for neuron in self.presynapticNeurons:
            timeDelta = abs(time-neuron.lastSpikeTime)
            if timeDelta <= 20 and neuron.lastSpikeTime != -1:
                #presynaptic neuron spiked within the 20 TU's before the neuron spiked
                weightDelta = -self.aplus * math.exp(abs(time - neuron.lastSpikeTime) / self.tplus)
                self.presynapticWeights[neuron] += self.phi * weightDelta * (self.presynapticWeights[neuron] - abs(self.wmin))

    def reward(self, timeDelta, neuron):
        weightDelta = self.aminus * math.exp(timeDelta / self.tminus)
        self.presynapticWeights[neuron] += self.phi * weightDelta * (self.wmax - self.presynapticWeights[neuron])


    def penalty(self, timeDelta, neuron):
        weightDelta = -self.aplus * math.exp(-timeDelta / self.tplus)
        self.presynapticWeights[neuron] += self.phi * weightDelta * (self.presynapticWeights[neuron] - abs(self.wmin))

    def checkForWeightChange(self, time, neuron):
        if abs(time - self.lastSpikeTime) <= 20 and self.lastSpikeTime > -1:
            # the spike which called the checkForWeightChange method is indeed within the 20 TU period after
            # this neuron fired, so there is a penalty
            weightDelta = self.aminus * math.exp((time - self.lastSpikeTime) / self.tminus)
            self.presynapticWeights[neuron] += self.phi * weightDelta * (self.wmax - self.presynapticWeights[neuron])

