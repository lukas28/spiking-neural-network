'''
Name: Lukas Hubl
Version: 1.0
Purpose: In this file all the globally accessible parameters are defined
'''

filepath = '/Users/lukashubl/mnist_png/training/'   # path where the images are stored
trainingEpochs = 10                                 # the number of epochs to train the network
samplesPerDigit = 1                                 # the amount of different image instances per digit used for learning
outputNeurons = 10                                  # the number of output spike trains
simulationTime = 150                                # how many timesteps the training process should take per epoch
timeStep = 1                                        # the time delta inbetween timesteps of the simulationTime