

from SimplifiedNeuron import SimplyfiedNeuron
from numpy import *
import numpy as np
from random import randrange
import matplotlib.pyplot as plt
from ReceptiveField import ReceptiveField
import imageio
from SpikeTrain import SpikeTrainFactory
import Hyperparameters as hyp
import os



def depressNonSpikingNeurons(neurons, winnerNeuron):
    for ctr, neuron in enumerate(neurons):
        if neuron != winnerNeuron:
            neuron.membranePotential = 0




# loading the weights
f = open('weights.txt', 'r')

content = f.read()

f.close()

weights = content.split('\n')




rf = ReceptiveField()
st = SpikeTrainFactory(receptiveField=rf, duration = hyp.simulationTime, timestep = hyp.timeStep)

tester = []


firstLayer = []
secondLayer = []

for i in range(28*28):
    neuron = SimplyfiedNeuron()
    firstLayer.append(neuron)

for x in range(hyp.outputNeurons):
    neuron = SimplyfiedNeuron()
    relevantWeights = weights[x].split(',')
    for ctr, n in enumerate(firstLayer):
        neuron.addPresynapticNeuron(n)
        neuron.addPresynapticNeuronWithWeight(n, float(relevantWeights[ctr]))
    secondLayer.append(neuron)


number = 9

im = imageio.imread(str(hyp.filepath) + '/' + str(number) +'/6.png')
spikeTrain = st.generate(im)

for neuron in secondLayer:
    neuron.membranePotential = 0
    neuron.lastSpikeTime = -1
    neuron.isSpiking = 0
for neuron in firstLayer:
    neuron.lastSpikeTime = -1
    neuron.isSpiking = 0

timespan = hyp.simulationTime
timedelta = hyp.timeStep


spikeOccured = False

timeframe = arange(0, timespan+timedelta, timedelta)

f_spike = 0
winner = -1


for step, time in enumerate(timeframe):

    #print('input ...')
    for ctr, n in enumerate(firstLayer):
        if spikeTrain[ctr][step] == 1:
            n.isSpiking = 1
            n.lastSpikeTime = time
            #for neuron in secondLayer:
                #neuron.checkForWeightChange(time, n) # penalty for beeing too late?
        else:
            n.isSpiking = 0


    #print('update potential ...')
    #iterate over all neurons in the second layer
    for i,neu in enumerate(secondLayer):
        #update the neuron potential
        synapses = []
        for item in neu.presynapticNeurons:
            synapses.append(neu.presynapticWeights[item])
        neu.updatePotential(time, spikeTrain[:, step], synapses)

    if f_spike == 0:
        highestPot = -1
        winnerNeuron = None
        winnerIndex = -1
        winnerPotential = -1
        for index, neuron in enumerate(secondLayer):
            if neuron.membranePotential > highestPot:
                highestPot = neuron.membranePotential
                winnerNeuron = neuron
                winnerIndex = index
                winnerPotential = neuron.membranePotential

        if winnerNeuron.checkForSpike(time):
            depressNonSpikingNeurons(neurons=secondLayer, winnerNeuron=winnerNeuron)
            f_spike = 1
            print('winner is ' + str(winnerIndex))


for ctr, neuron in enumerate(secondLayer):
    print('neuron ' + str(ctr) + ' spiked ' + str(neuron.spikeCount) + str(' times'))














