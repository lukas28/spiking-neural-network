'''
Name: Lukas Hubl
Version: 1.0
Purpose: This code defines how the input neurons convert input images into spike trains
To generate a spiketrain from an input image a receptive field is required. This field defines how the
# network 'sees' the environment it should respond to, in this case it is a 28x28 pixel image
'''

from scipy import signal
import numpy as np
import math

class SpikeTrainFactory:

    def __init__(self, receptiveField, duration, timestep):
        self.receptiveField = receptiveField
        self.duration = duration
        self.timestep = timestep

    def generate(self, image):
        #grad = signal.convolve2d(image, self.receptiveField.receptiveFieldMatrix, boundary='symm', mode='same')
        # print(grad)
        # plt.imshow(np.absolute(grad), cmap='gray')
        # plt.show()

        pot = np.zeros([image.shape[0], image.shape[1]])
        ran = [-2, -1, 0, 1, 2]
        ox = 2
        oy = 2

        # Convolution
        for i in range(image.shape[0]):
            for j in range(image.shape[1]):
                summ = 0
                for m in ran:
                    for n in ran:
                        if (i + m) >= 0 and (i + m) <= image.shape[0] - 1 and (j + n) >= 0 and (j + n) <= image.shape[
                            0] - 1:
                            summ = summ + self.receptiveField.receptiveFieldMatrix[ox + m][oy + n] * image[i + m][j + n] / 255
                pot[i][j] = summ

        train = self.spikeTrainGeneration(pot)
        return train


    def generateSpikingResponse(self, recepNeuronResponse):

        rpmax = 10      # min refractory period
        maxr = 100      # maximal mambrane potential


        output = np.zeros([16, 16])
        for x in range(recepNeuronResponse.shape[0]):
            for y in range(recepNeuronResponse.shape[1]):
                if recepNeuronResponse[x][y] <= 0:
                    output[x][y] = 0
                else:
                    output[x][y] = 1/(rpmax * (recepNeuronResponse[x][y]/maxr))

        #print(output)
        train = self.spikeTrainGeneration(output)
        return train

    def spikeTrainGeneration(self, neuralResponse):
        # initializing spike train
        train = []

        for l in range(neuralResponse.shape[0]):
            for m in range(neuralResponse.shape[1]):

                temp = np.zeros([(self.duration + 1), ])

                # calculating firing rate proportional to the membrane potential
                freq = np.interp(neuralResponse[l][m], [-1.069, 2.781], [1, 20])
                # print(pot[l][m], freq)
                # print freq

                assert freq > 0

                freq1 = math.ceil(600 / freq)

                # generating spikes according to the firing rate
                k = freq1
                if (neuralResponse[l][m] > 0):
                    while k < (self.duration + 1):
                        temp[int(k)] = 1
                        k = k + freq1
                train.append(temp)
                # print sum(temp)
        return np.array(train)