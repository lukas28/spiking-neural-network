# This class represents the receptive field of the input image
import random


global imageLength
global imageWidth




class ReceptiveField:



    # initializing a receptive field - manhatten distance is used
    def __init__(self):
        l1 = 0.625
        l2 = 0.125
        l3 = -0.125
        l4 = -0.5

        self.receptiveFieldMatrix = [[l4,l3,l2,l3,l4],
                                     [l3,l2,l1,l2,l3],
                                     [l2,l1,1,l1,l2],
                                     [l3,l2,l1,l2,l3],
                                     [l4,l3,l2,l3,l4]]








